const main = require('./handlers/main.js');
const article = require('./handlers/article.js');
const auth = require('./handlers/authentication.js');

function registerUrls(app) {
    app.get('/', main.showMain);
    app.get('/articles/:id', article.showArticle);
    app.post('/login-form', auth.propagateUser);
    app.get('/logout', auth.logout);
}

exports.registerUrls = registerUrls;