const knex = require('./database.js').knex;;
const comments = require('./comments.js');
const tags = require('./tags.js');

function showArticle(req, res) {
    readData(req.params.id, function(article) {
        res.render('article', article)
    });
}

/**
 * Reads article and referenced data for the passed article id.
 * @param articleId id of the require article
 * @param callback callback function which accepts the read article
 */
function readData(articleId, callback) {
    knex('eleven_article').where('id', articleId).then(function (rows) {
        let article = rows[0];

        let parts = splitDescriptionAndBody(article.content);
        article.description = parts[0];
        article.rest_content = parts[1];

        readAuthors(article, function(authors) {
            article.authors = authors;
            readTags(article, function(tags) {
                article.tags = tags;
                comments.readComments(articleId,  22, function (comments) {
                    article.comments = comments;
                    callback(article);
                });
            });
        });
    });
}


/**
 * Reads authors for the passed article and passes them to the provided callback function.
 * @param article article to get authors for
 * @param callback callback function which accepts read authors
 */
function readAuthors(article, callback) {
    knex('auth_user')
        .join('eleven_article_author', 'auth_user.id', '=', 'eleven_article_author.user_id')
        .where('article_id', article.id)
        .select('auth_user.username', 'auth_user.first_name', 'auth_user.last_name')
        .then(function (rows) {
            rows.forEach(function(item) {
                item.getFullName = function () {
                    return this.first_name + ' ' + this.last_name;
                }
            });
            callback(rows);
        });
}

// This tag is a common approach to separate articles description from its actual content.
const MORE_TAG = '[more]';
// Double line break is another rare approach to separate description and actual content
const MORE_REGEXP = new RegExp('(.+)\r\n\r\n');

/**
 * Splits article content in 2 parts: description and the rest content depending on special symbols such as [more] and '\r\n\r\n'.
 * @param content raw content of the article
 * @returns {[*,*]} array of 2 elements: [0] - description, [1] - article body
 */
function splitDescriptionAndBody(content) {
    // TODO: strip_tags from content
    let parts = content.split(MORE_TAG);
    if (parts.length > 1) {
        return [parts[0], parts[1]];
    }
    let match = content.match(MORE_REGEXP);

    let description = match ?  match[0] : '';
    return [description, content.substring(description.length)];
}


/**
 * Reads tags for the passed article and passes them to the provided callback function.
 * @param article article to get tags for
 * @param callback callback function which accepts read article tags
 */
function readTags(article, callback) {
    tags.readArticleTags(article.id, callback);
}


exports.showArticle = showArticle;