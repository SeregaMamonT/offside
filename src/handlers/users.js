const knex = require('./database.js').knex;

function readUser(options, callback) {
    knex('auth_user')
        .where(options)
        .then(function(rows) {
            let user = rows.length > 0 ? rows[0] : undefined;
            callback(user);
        });
}

exports.readUser = readUser;