var knex = require('./database.js').knex;

/**
 * Reads tags for the passed object.
 * @param objectId id of the object to get tags for
 * @param contentType type of the object
 * @param callback callback function to accept read tags
 */
function readTags(objectId, contentType, callback) {
    knex('eleven_materializedtag')
        .join('eleven_tagged', 'eleven_materializedtag.id', '=', 'eleven_tagged.tag_id')
        .where({'eleven_tagged.object_id': objectId, 'eleven_tagged.content_type_id': contentType})
        .select('name', 'slug')
        .then(function(rows) {
            callback(rows);
        });
}


/**
 * Reads tags for the passed article.
 * @param articleId id of the article to get tags for
 * @param callback callback function to accept read tags
 */
function readArticleTags(articleId, callback) {
    readTags(articleId, 22, callback);
}

exports.readTags = readTags;
exports.readArticleTags = readArticleTags;