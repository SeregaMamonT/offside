const users = require("./users.js");

function propagateUser(req, res) {
    let username = req.body.username;
    users.readUser({ username: username }, function (user) {
        if (user) {
            req.session.regenerate(function () {
                req.session.user = user;
                res.redirect(req.query.next);
            });
        }
    });
}


function logout(req, res) {
    req.session.destroy(function() {
        res.redirect(req.query.next)    ;
    });
}

exports.propagateUser = propagateUser;
exports.logout = logout;