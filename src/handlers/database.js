var db_config = require('../settings.js').db_config;

var knex = require('knex')({
    client: 'mysql',
    connection: db_config
});

exports.knex = knex;