const knex = require('./database.js').knex;

function readComments(objectId, contentType, callback) {
    knex('custom_comments_app_mpttcomment')
        .join('django_comments', 'custom_comments_app_mpttcomment.comment_ptr_id', '=', 'django_comments.id')
        .where({
            'django_comments.site_id': 1,
            'django_comments.object_pk': objectId,
            'django_comments.content_type_id': contentType,
            'django_comments.is_public': 1,
            'django_comments.is_removed': 0
        })
        .orderBy('submit_date', 'asc')
        .then(function(rows) {
            let root = { id: null, children: []};
            rows.forEach(item => addNode(root, item));
            callback(root);
        });
}

function addNode(root, node) {
    if (root.id == node.parent_id) {
        root.children.push(node);
        node.children = [];
        return true;
    }
    return root.children.some(item => addNode(item, node));
}

exports.readComments = readComments;