var props = require('./properties.js');

var db_config = {
    host     : props.db_host,
    user     : props.db_user,
    password : props.db_password,
    database : props.db_database
};

exports.db_config = db_config;