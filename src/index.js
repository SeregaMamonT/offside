const express = require('express');
const ejsLayouts = require('express-ejs-layouts');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const handlers = require('./handlers.js');

const app = express();

// Setting view engine
app.set('view engine', 'ejs');
app.use(ejsLayouts);
// Static
app.use(express.static(__dirname + '/static'));
app.use(bodyParser());
app.use(cookieParser('messi is hobbit'));
app.use(session());

app.use(function(req, res, next) {
    res.locals.req = req;
    next();
});

// Handlers
handlers.registerUrls(app);

// global libs
app.locals.moment = require('moment');
app.locals.moment.locale('ru');

// Server start
app.listen(1337, function () {
    console.log('Started');
});
